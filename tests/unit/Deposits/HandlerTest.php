<?php declare(strict_types=1);

namespace Tests\Unit\Deposits;

use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Handlers\DepositReceivedHandler;
use Paycoiner\Client\Models\Webhooks\DepositReceived;
use Tests\TestCase;

class HandlerTest extends TestCase
{
    public function getHandler(): DepositReceivedHandler
    {
        return new DepositReceivedHandler(static::WEBHOOK_KEY);
    }
    /**
     * @group handler
     * @group deposit
     */
    public function testSuccessfulDepositReceivedHandler()
    {
        $handler = $this->getHandler();
        $mockData = json_decode($this->getMockData('deposits/successful.json'), true);
        /** @var DepositReceived $depositReceived */
        $depositReceived = $handler->handle($mockData);

        $this->assertEquals($mockData['depositId'], $depositReceived->getDepositId());
        $this->assertEquals($mockData['customerId'], $depositReceived->getCustomerId());
        $this->assertEquals($mockData['txId'], $depositReceived->getTxId());
        $this->assertEquals($mockData['address'], $depositReceived->getAddress());
        $this->assertEquals($mockData['amount'], $depositReceived->getAmount());
        $this->assertEquals($mockData['currency'], $depositReceived->getCurrency());
        $this->assertEquals($mockData['confirmations'], $depositReceived->getConfirmations());
    }
    /**
     * @group handler
     * @group deposit
     */
    public function testDepositReceivedHandlerWithIncorrectData()
    {
        $this->expectException(InvalidHash::class);

        $clientMock = $this->getHandler();
        $clientMock->handle([]);
    }

    /**
     * @group handler
     * @group deposit
     */
    public function testDepositReceivedHandlerWithIncorrectHash()
    {
        $this->expectException(InvalidHash::class);

        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('deposits/incorrect-hash.json'), true));
    }

    /**
     * @group handler
     * @group deposit
     */
    public function testDepositReceivedHandlerWithCorrectHashAndIncorrectData()
    {
        $this->expectException(ValidationException::class);

        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('webhook-incorrect-data.json'), true));
    }
}
