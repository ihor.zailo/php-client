<?php declare(strict_types=1);

namespace Tests\Unit\Payouts;

use Paycoiner\Client\Exceptions\ValidationException;
use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Handlers\PayoutStatusHandler;
use Tests\TestCase;

class StatusHandlerTest extends TestCase
{
    public function getHandler(): PayoutStatusHandler
    {
        return new PayoutStatusHandler(static::WEBHOOK_KEY);
    }

    /**
     * @group handler
     * @group payout
     */
    public function testSuccessfulPayoutStatusHandler()
    {
        $handler = $this->getHandler();
        $mockData = json_decode($this->getMockData('payouts/status-successful.json'), true);
        /** @var \Paycoiner\Client\Models\Webhooks\PayoutStatus $payoutStatus */
        $payoutStatus = $handler->handle($mockData);

        $this->assertEquals($mockData['payoutId'], $payoutStatus->getPayoutId());
        $this->assertEquals($mockData['customerId'], $payoutStatus->getCustomerId());
        $this->assertEquals($mockData['orderId'], $payoutStatus->getOrderId());
        $this->assertEquals($mockData['fee'], $payoutStatus->getFee());
    }

    /**
     * @group handler
     * @group payout
     */
    public function testPayoutStatusHandlerWithIncorrectData()
    {
        $this->expectException(InvalidHash::class);

        $clientMock = $this->getHandler();
        $clientMock->handle([]);
    }

    /**
     * @group handler
     * @group payout
     */
    public function testPayoutStatusHandlerWithIncorrectHash()
    {
        $this->expectException(InvalidHash::class);

        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('payouts/incorrect-hash.json'), true));
    }

    /**
     * @group handler
     * @group payout
     */
    public function testPayoutStatusHandlerWithCorrectHashAndIncorrectData()
    {
        $this->expectException(ValidationException::class);

        $clientMock = $this->getHandler();
        $clientMock->handle(json_decode($this->getMockData('webhook-incorrect-data.json'), true));
    }
}
