<?php declare(strict_types=1);

namespace Paycoiner\Client\Clients;

use GuzzleHttp\Exception\GuzzleException;
use Paycoiner\Client\Enums\HttpMethod;
use Paycoiner\Client\Exceptions\Endpoints\UnprocessableRequest;
use Paycoiner\Client\Models\Requests\AddressValidateRequest;

class AddressValidatorClient extends Client
{
    const API_VERSION_PREFIX = 'api';

    /**
     * @return bool|null
     *
     * @throws GuzzleException
     * @throws UnprocessableRequest
     * @throws \Paycoiner\Client\Exceptions\PaycoinerClientException
     */
    public function validate(AddressValidateRequest $request)
    {
        $result = $this->send(HttpMethod::POST(), self::API_VERSION_PREFIX . '/address/validate', $request->toArray());

        return $result->getResponse()['isValid'];
    }

    protected function appendHashToRequest(&$data, &$headers)
    {
        // Service doesn't have authorization.
    }
}
