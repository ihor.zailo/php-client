<?php declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Models\Webhooks\DepositReceived;
use Paycoiner\Client\Services\HashService;

class DepositReceivedHandler extends Handler
{
    /** @var HashService */
    protected $hashService;

    public function __construct(string $webhookKey)
    {
        parent::__construct($webhookKey);
        $this->hashService = new HashService();
    }

    /**
     * @throws InvalidHash
     * @throws \Paycoiner\Client\Exceptions\ValidationException
     */
    function handle(array $data): DepositReceived
    {
        if (isset($data['hash']) === false) {
            throw new InvalidHash();
        }
        if ($this->hashService->isInvalidHash($data, $this->webhookKey, $data['hash'])) {
            throw new InvalidHash();
        }

        return DepositReceived::fromArray($data);
    }
}
