<?php declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

abstract class Handler
{
    /** @var string */
    protected $webhookKey;

    public function __construct(string $webhookKey)
    {
        $this->webhookKey = $webhookKey;
    }

    abstract function handle(array $data);
}
