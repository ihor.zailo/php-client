<?php declare(strict_types=1);

namespace Paycoiner\Client\Handlers;

use Paycoiner\Client\Exceptions\Webhooks\InvalidHash;
use Paycoiner\Client\Models\Responses\Invoice;
use Paycoiner\Client\Services\HmacService;

class PaymentHandler extends Handler
{
    /** @var HmacService */
    protected $hmacService;

    public function __construct(string $webhookKey)
    {
        parent::__construct($webhookKey);
        $this->hmacService = new HmacService();
    }

    /**
     * @throws InvalidHash
     * @throws \Paycoiner\Client\Exceptions\ValidationException
     */
    function handle(array $data): Invoice
    {
        if (false === isset($_SERVER['HTTP_HMAC'])) {
            throw new InvalidHash();
        }
        $hmac = $_SERVER['HTTP_HMAC'];
        if (false === $this->hmacService->validateHmac($hmac, $this->webhookKey, $data)) {
            throw new InvalidHash();
        }

        return Invoice::fromArray($data);
    }
}
