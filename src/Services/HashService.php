<?php declare(strict_types=1);

namespace Paycoiner\Client\Services;

class HashService
{
    public function isInvalidHash(array $data, string $customerKey, string $hash): bool
    {
        return false === $this->isValidHash($data, $customerKey, $hash);
    }

    public function isValidHash(array $data, string $customerKey, string $hash): bool
    {
        return $this->hash($data, $customerKey) === $hash;
    }

    public function hash(array $data, string $customerKey): string
    {
        $params = [];
        foreach ($data as $key => $value) {
            if ($key === 'hash') {
                continue;
            }
            if (is_array($value)) {
                $value = json_encode($value);
            }
            $params[] = $value;
        }
        $params[] = $customerKey;

        return hash('sha256', implode('|', $params));
    }
}
