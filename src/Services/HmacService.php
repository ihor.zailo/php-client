<?php declare(strict_types=1);

namespace Paycoiner\Client\Services;

class HmacService
{
    public function getHmac(string $payload, string $key): string
    {
        return hash_hmac('sha256', $payload, $key);
    }

    public function validateHmac(string $hash, string $key, array $payload): bool
    {
        return hash_equals($this->getHmac(json_encode($payload), $key), $hash);
    }
}
